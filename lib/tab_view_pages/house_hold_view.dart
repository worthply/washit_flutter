import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/model/mens_item_model.dart';
import 'package:washit/utils/my_colors.dart';

class HouseHoldView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HouseHoldViewState();
  }
}

class HouseHoldViewState extends State<HouseHoldView> {
  List<MenItemModel> itemList = List();
  int _itemCount = 0;

  @override
  void initState() {
    super.initState();

    itemList.add(MenItemModel(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBBoap4aDSrqcsS7PgDLaFd5cfqYEWIzYxdLNbn3VHexdGFK-qTQ&s",
        "Shirt",
        "5.00"));

    itemList.add(MenItemModel(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBBoap4aDSrqcsS7PgDLaFd5cfqYEWIzYxdLNbn3VHexdGFK-qTQ&s",
        "Shirt",
        "6.50"));
    itemList.add(MenItemModel(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBBoap4aDSrqcsS7PgDLaFd5cfqYEWIzYxdLNbn3VHexdGFK-qTQ&s",
        "Shirt",
        "5.00"));
    itemList.add(MenItemModel(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBBoap4aDSrqcsS7PgDLaFd5cfqYEWIzYxdLNbn3VHexdGFK-qTQ&s",
        "Shirt",
        "5.00"));
    itemList.add(MenItemModel(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBBoap4aDSrqcsS7PgDLaFd5cfqYEWIzYxdLNbn3VHexdGFK-qTQ&s",
        "Shirt",
        "5.00"));
    itemList.add(MenItemModel(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBBoap4aDSrqcsS7PgDLaFd5cfqYEWIzYxdLNbn3VHexdGFK-qTQ&s",
        "Shirt",
        "5.00"));
    itemList.add(MenItemModel(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBBoap4aDSrqcsS7PgDLaFd5cfqYEWIzYxdLNbn3VHexdGFK-qTQ&s",
        "Shirt",
        "5.00"));
    itemList.add(MenItemModel(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBBoap4aDSrqcsS7PgDLaFd5cfqYEWIzYxdLNbn3VHexdGFK-qTQ&s",
        "Shirt",
        "5.00"));
    itemList.add(MenItemModel(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBBoap4aDSrqcsS7PgDLaFd5cfqYEWIzYxdLNbn3VHexdGFK-qTQ&s",
        "Shirt",
        "5.00"));
    itemList.add(MenItemModel(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBBoap4aDSrqcsS7PgDLaFd5cfqYEWIzYxdLNbn3VHexdGFK-qTQ&s",
        "Shirt",
        "5.00"));
    itemList.add(MenItemModel(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBBoap4aDSrqcsS7PgDLaFd5cfqYEWIzYxdLNbn3VHexdGFK-qTQ&s",
        "Shirt",
        "5.00"));
    itemList.add(MenItemModel(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBBoap4aDSrqcsS7PgDLaFd5cfqYEWIzYxdLNbn3VHexdGFK-qTQ&s",
        "Shirt",
        "5.00"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: basketView(),
      backgroundColor: MyColors.unSelectedIndicatorColor,
      body: mensView(),
    );
  }

  // make view for basket count
  Widget basketView() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(top: 10, bottom: 10, left: 16, right: 16),
      color: MyColors.bagItemColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Your Basket',
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: MyColors.whiteColor,
                    fontSize: ScreenUtil().setSp(20),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  '25 Items added',
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: MyColors.whiteColor.withOpacity(0.6),
                    fontSize: ScreenUtil().setSp(14),
                  ),
                ),
              ],
            ),
          ),
          Text(
            '\$99 + 17.99 Tax',
            style: TextStyle(
              fontWeight: FontWeight.normal,
              color: MyColors.whiteColor,
              fontSize: ScreenUtil().setSp(18),
            ),
          ),
        ],
      ),
    );
  }

  // make view for men's
  Widget mensView() {
    return ListView.builder(
        physics: BouncingScrollPhysics(),
        itemBuilder: (context, index) {
          return mensItemRow(itemList, index);
        },
        itemCount: itemList.length);
  }

  // make item row for men's view
  Widget mensItemRow(List<MenItemModel> itemList, int index) {
    return Container(
      margin: EdgeInsets.only(top: 1.2, bottom: 1.2),
      padding: EdgeInsets.only(top: 8, bottom: 8, right: 10, left: 10),
      decoration: BoxDecoration(color: MyColors.whiteColor),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              height: 60,
              width: 60,
              imageUrl: itemList[index].image,
              placeholder: (context, url) => new CircularProgressIndicator(
                strokeWidth: 1,
              ),
              errorWidget: (context, url, error) => new Icon(
                Icons.error,
                size: 60,
                color: Colors.grey,
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  itemList[index].itemName,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: MyColors.blackColor,
                      fontSize: ScreenUtil().setSp(18)),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  '2 X \$${itemList[index].itemPrice}',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: MyColors.itemPriceColor,
                      fontSize: ScreenUtil().setSp(14)),
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () {
              setState(() {
                if (_itemCount != 0) {
                  _itemCount = _itemCount - 1;
                }
              });
            },
            child: Image(
              image: AssetImage('assets/icons/minus.png'),
              fit: BoxFit.cover,
              height: 22,
              width: 22,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            _itemCount.toString(),
            style: TextStyle(
                fontSize: ScreenUtil().setSp(18),
                fontWeight: FontWeight.normal,
                color: MyColors.blackColor),
          ),
          SizedBox(
            width: 10,
          ),
          InkWell(
            onTap: () {
              setState(() {
                _itemCount = _itemCount + 1;
              });
            },
            child: Image(
              image: AssetImage('assets/icons/plus.png'),
              fit: BoxFit.cover,
              height: 22,
              width: 22,
            ),
          ),
          SizedBox(
            width: 8,
          ),
        ],
      ),
    );
  }
}
