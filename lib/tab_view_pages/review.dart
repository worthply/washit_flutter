import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:washit/utils/my_colors.dart';

class Review extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ReviewState();
  }
}

class ReviewState extends State<Review> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: reviewView(),
    );
  }

  // make view for review
  Widget reviewView() {
    return Container(
      margin: EdgeInsets.only(top: 3),
      decoration: BoxDecoration(color: MyColors.whiteColor),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: MyColors.unSelectedIndicatorColor.withOpacity(0.2),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      '4.1',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(25),
                          fontWeight: FontWeight.bold,
                          color: MyColors.blackColor.withOpacity(0.9)),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: RatingBar(
                        unratedColor: MyColors.greyColor,
                        initialRating: 4,
                        minRating: 0,
                        itemSize: 25,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemPadding: EdgeInsets.symmetric(horizontal: 3.0),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                      ),
                    ),
                    Text(
                      '429 Review',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(16),
                          fontWeight: FontWeight.normal,
                          color: MyColors.blackColor.withOpacity(0.8)),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      '5 star',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(14),
                          fontWeight: FontWeight.normal,
                          color: MyColors.blackColor.withOpacity(0.8)),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    LinearPercentIndicator(
                      width: MediaQuery.of(context).size.width - 90,
                      lineHeight: 6.0,
                      percent: 0.8,
                      backgroundColor: MyColors.unSelectedIndicatorColor,
                      progressColor: MyColors.ratingPercentColor,
                    )
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      '4 star',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(14),
                          fontWeight: FontWeight.normal,
                          color: MyColors.blackColor.withOpacity(0.8)),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    LinearPercentIndicator(
                      width: MediaQuery.of(context).size.width - 90,
                      lineHeight: 6.0,
                      percent: 0.7,
                      backgroundColor: MyColors.unSelectedIndicatorColor,
                      progressColor: MyColors.ratingPercentColor,
                    )
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      '3 star',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(14),
                          fontWeight: FontWeight.normal,
                          color: MyColors.blackColor.withOpacity(0.8)),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    LinearPercentIndicator(
                      width: MediaQuery.of(context).size.width - 90,
                      lineHeight: 6.0,
                      percent: 0.2,
                      backgroundColor: MyColors.unSelectedIndicatorColor,
                      progressColor: MyColors.ratingPercentColor,
                    )
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      '2 star',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(14),
                          fontWeight: FontWeight.normal,
                          color: MyColors.blackColor.withOpacity(0.8)),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    LinearPercentIndicator(
                      width: MediaQuery.of(context).size.width - 90,
                      lineHeight: 6.0,
                      percent: 0.1,
                      backgroundColor: MyColors.unSelectedIndicatorColor,
                      progressColor: MyColors.ratingPercentColor,
                    )
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      '1 star',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(14),
                          fontWeight: FontWeight.normal,
                          color: MyColors.blackColor.withOpacity(0.8)),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    LinearPercentIndicator(
                      width: MediaQuery.of(context).size.width - 90,
                      lineHeight: 6.0,
                      percent: 0.4,
                      backgroundColor: MyColors.unSelectedIndicatorColor,
                      progressColor: MyColors.ratingPercentColor,
                    )
                  ],
                )
              ],
            ),
          ),
          Expanded(
              child: ListView.separated(
                  separatorBuilder: (context, index) => Divider(
                        color: Colors.grey.withOpacity(0.7),
                        height: 1,
                      ),
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemCount: 2,
                  itemBuilder: (context, index) {
                    return reviewCommentItemRow(index);
                  }))
        ],
      ),
    );
  }

  // make row for review comment
  Widget reviewCommentItemRow(int index) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 10, top: 10, bottom: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Text(
                  'Cirs Stankovic',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ScreenUtil().setSp(18),
                      color: MyColors.blackColor.withOpacity(0.9)),
                ),
              ),
              RatingBar(
                unratedColor: MyColors.greyColor,
                initialRating: 4,
                minRating: 0,
                itemSize: 20,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(horizontal: 0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
            textAlign: TextAlign.start,
            style: TextStyle(
                fontWeight: FontWeight.normal,
                color: MyColors.blackColor.withOpacity(0.8),
                fontSize: ScreenUtil().setSp(14)),
          )
        ],
      ),
    );
  }
}
