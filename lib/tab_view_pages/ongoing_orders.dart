import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/ui/order_details_screen.dart';
import 'package:washit/utils/my_colors.dart';

class OnGoingOrdersScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return OnGoingOrdersScreenState();
  }
}

class OnGoingOrdersScreenState extends State<OnGoingOrdersScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: onGoingOrdersView(),
    );
  }

  // make view for onGoing Orders
  Widget onGoingOrdersView() {
    return ListView.builder(
        itemCount: 10,
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.only(top: 8, bottom: 8, right: 8, left: 8),
        itemBuilder: (context, index) {
          return onGoingOrdersRow(index);
        });
  }

  // make row for ongoing  orders
  Widget onGoingOrdersRow(int index) {
    return Card(
        elevation: 3,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, top: 16, bottom: 16),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text(
                        'Order No - 110040717',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(18),
                            fontWeight: FontWeight.w600,
                            color: MyColors.blackColor),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Order delivered successfully',
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: MyColors.orderSuccessColor,
                            fontSize: ScreenUtil().setSp(16)),
                      ),
                    ],
                  )),
                  Text(
                    '\$474',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        color: MyColors.blackColor,
                        fontWeight: FontWeight.w600),
                  )
                ],
              ),
            ),
            Container(
              width: double.infinity,
              height: 0.7,
              color: MyColors.greyColor,
            ),
            Container(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              decoration: BoxDecoration(
                  color: MyColors.whiteColor,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8),
                  )),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image(
                          image: AssetImage(index % 2 == 0
                              ? 'assets/icons/confirmed.png'
                              : 'assets/icons/confirmed_blur.png')),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Confirmed',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(10),
                            color: MyColors.blackColor.withOpacity(0.8),
                            fontWeight: FontWeight.w800),
                      )
                    ],
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: Container(
                      height: 1,
                      color: MyColors.greyColor,
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image(
                        image: AssetImage(index % 2 == 0
                            ? 'assets/icons/pickup.png'
                            : 'assets/icons/pickup_blur.png'),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Picked up',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(10),
                            color: MyColors.blackColor.withOpacity(0.8),
                            fontWeight: FontWeight.w800),
                      )
                    ],
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: Container(
                      height: 1,
                      color: MyColors.greyColor,
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image(
                          image: AssetImage(index % 2 == 0
                              ? 'assets/icons/in_process.png'
                              : 'assets/icons/inprocess_blur.png')),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'In Process',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(10),
                            color: MyColors.blackColor.withOpacity(0.8),
                            fontWeight: FontWeight.w800),
                      )
                    ],
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: Container(
                      height: 1,
                      color: MyColors.greyColor,
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image(
                          image: AssetImage(index % 2 == 0
                              ? 'assets/icons/shipped.png'
                              : 'assets/icons/shipped_blur.png')),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Shipped',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(10),
                            color: MyColors.blackColor.withOpacity(0.8),
                            fontWeight: FontWeight.w800),
                      )
                    ],
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: Container(
                      height: 1,
                      color: MyColors.greyColor,
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image(
                          image: AssetImage(index % 2 == 0
                              ? 'assets/icons/success_order.png'
                              : 'assets/icons/delivered_blur.png')),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Delivered',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(10),
                            color: MyColors.blackColor.withOpacity(0.8),
                            fontWeight: FontWeight.w800),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: double.infinity,
              height: 0.7,
              color: MyColors.greyColor,
            ),
            Container(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 16, bottom: 16),
              decoration: BoxDecoration(
                  color: MyColors.whiteColor,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8),
                  )),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding:
                        EdgeInsets.only(left: 16, right: 16, top: 5, bottom: 5),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border:
                            Border.all(color: MyColors.blackColor, width: 1)),
                    child: Text(
                      'Cancel Order',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(16),
                          fontWeight: FontWeight.w500,
                          color: MyColors.messageColor),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => OrderDetailsScreen()),
                      );
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 16, right: 16, top: 5, bottom: 5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border:
                              Border.all(color: MyColors.blackColor, width: 1)),
                      child: Text(
                        'View Details',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(16),
                            fontWeight: FontWeight.w500,
                            color: MyColors.messageColor),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ));
  }
}
