import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/ui/order_details_screen.dart';
import 'package:washit/utils/my_colors.dart';

class PastOrderScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PastOrderScreenState();
  }
}

class PastOrderScreenState extends State<PastOrderScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pastOrderView(),
    );
  }

  // make view for past orders
  Widget pastOrderView() {
    return ListView.builder(
        itemCount: 10,
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.only(top: 8, bottom: 8, right: 8, left: 8),
        itemBuilder: (context, index) {
          return pastOrdersRow(index);
        });
  }

  // make row for past orders
  Widget pastOrdersRow(int index) {
    return Card(
        elevation: 2,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, top: 16, bottom: 16),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text(
                        'Order No - 110040717',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(18),
                            fontWeight: FontWeight.w600,
                            color: MyColors.blackColor),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Order delivered successfully',
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: MyColors.orderSuccessColor,
                            fontSize: ScreenUtil().setSp(16)),
                      ),
                    ],
                  )),
                  Text(
                    '\$474',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        color: MyColors.blackColor,
                        fontWeight: FontWeight.w600),
                  )
                ],
              ),
            ),
            Container(
              width: double.infinity,
              height:0.7,
              color: MyColors.greyColor,
            ),
            Container(
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                  color: MyColors.whiteColor,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8),
                  )),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: RatingBar(
                      tapOnlyMode: false,
                      unratedColor: MyColors.greyColor,
                      initialRating: 2,
                      minRating: 0,
                      itemSize: 25,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                      MaterialPageRoute(builder: (context)=>OrderDetailsScreen()),);
                    },
                    child: Container(
                      padding:
                          EdgeInsets.only(left: 16, right: 16, top: 5, bottom: 5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border:
                              Border.all(color: MyColors.blackColor, width: 1)),
                      child: Text(
                        'View Details',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(16),
                            fontWeight: FontWeight.normal,
                            color: MyColors.messageColor),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ));
  }
}
