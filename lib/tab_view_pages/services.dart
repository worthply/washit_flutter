import 'package:flutter/material.dart';
import 'package:washit/utils/my_colors.dart';

class Services extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ServicesState();
  }
}

class ServicesState extends State<Services> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: servicesView(),
    );
  }

  // make view for services
  Widget servicesView() {
    return GridView.builder(
        shrinkWrap: true,
        itemCount: 6,
        padding: EdgeInsets.only(left: 5,right: 5,top: 5,bottom: 5),
        physics: ClampingScrollPhysics(),
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (context, index) {
          return servicesItem();
        });
  }

  // make row for services item view
  Widget servicesItem() {
    return Card(
      margin: EdgeInsets.only(right: 5,bottom: 5,top: 5,left: 5),
      elevation: 2,
      child: Container(
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage('assets/icons/rohit.jpeg'))),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.all(10),
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "Washup & Fold",
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "\$49/per kg",
                      style: TextStyle(
                          color: MyColors.itemPriceColor.withOpacity(0.9),
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
