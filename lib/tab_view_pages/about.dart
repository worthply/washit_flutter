import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/utils/my_colors.dart';

class About extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AboutState();
  }
}

class AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: aboutBottomView(),
      body: aboutView(),
    );
  }

  // make view for about us
  Widget aboutView() {
    return Container();
  }

  // make view for about bottom view
  Widget aboutBottomView() {
    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(color: MyColors.whiteColor, boxShadow: [
        BoxShadow(
          color: Colors.black,
          offset: Offset(1.0, 6.0),
          blurRadius: 8.0,
        ),
      ]),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Text(
                  'Mon to Fri',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: MyColors.blackColor.withOpacity(0.9),
                    fontSize: ScreenUtil().setSp(16),
                  ),
                ),
              ),
              Text(
                ':',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: MyColors.blackColor.withOpacity(0.9),
                  fontSize: ScreenUtil().setSp(16),
                ),
              ),
              SizedBox(width: 5),
              Expanded(
                flex: 3,
                child: Text(
                  '08:00 AM - 08:00 PM',
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: MyColors.messageColor,
                    fontSize: ScreenUtil().setSp(16),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Text(
                  'Saturday',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: MyColors.blackColor.withOpacity(0.9),
                    fontSize: ScreenUtil().setSp(16),
                  ),
                ),
              ),
              Text(
                ':',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: MyColors.blackColor.withOpacity(0.9),
                  fontSize: ScreenUtil().setSp(16),
                ),
              ),
              SizedBox(width: 5),
              Expanded(
                flex: 3,
                child: Text(
                  '08:00 AM - 08:00 PM',
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: MyColors.messageColor,
                    fontSize: ScreenUtil().setSp(16),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Text(
                  'Sunday',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: MyColors.blackColor.withOpacity(0.9),
                    fontSize: ScreenUtil().setSp(16),
                  ),
                ),
              ),
              Text(
                ':',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: MyColors.blackColor.withOpacity(0.9),
                  fontSize: ScreenUtil().setSp(16),
                ),
              ),
              SizedBox(width: 5),
              Expanded(
                flex: 3,
                child: Text(
                  'Closed',
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: MyColors.messageColor,
                    fontSize: ScreenUtil().setSp(16),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 25,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Text(
                  'Description',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: MyColors.blackColor.withOpacity(0.9),
                    fontSize: ScreenUtil().setSp(16),
                  ),
                ),
              ),
              Text(
                ':',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: MyColors.blackColor.withOpacity(0.9),
                  fontSize: ScreenUtil().setSp(16),
                ),
              ),
              SizedBox(width: 5),
              Expanded(
                flex: 3,
                child: Text(
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: MyColors.messageColor,
                    fontSize: ScreenUtil().setSp(16),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 25,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Text(
                  'Visit our website',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: MyColors.blackColor.withOpacity(0.9),
                    fontSize: ScreenUtil().setSp(16),
                  ),
                ),
              ),
              Text(
                ':',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: MyColors.blackColor.withOpacity(0.9),
                  fontSize: ScreenUtil().setSp(16),
                ),
              ),
              SizedBox(width: 5),
              Expanded(
                flex: 3,
                child: Text(
                  "www.appinnovation.in",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: MyColors.blueColor,
                    fontSize: ScreenUtil().setSp(16),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
