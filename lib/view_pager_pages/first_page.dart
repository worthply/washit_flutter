import 'package:flutter/material.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FirstPage extends StatefulWidget {
  PageController pageController;

  FirstPage(this.pageController);

  @override
  State<StatefulWidget> createState() {
    return FirstPageState();
  }
}

class FirstPageState extends State<FirstPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: firstPageView(),
    );
  }

  // make view for first page
  Widget firstPageView() {
    return Stack(
      children: <Widget>[
        Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/icons/splash.png'),
                  fit: BoxFit.cover)),
          child: Padding(
            padding: const EdgeInsets.only(left: 60, right: 60),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image(
                  image: AssetImage('assets/icons/intro_first.png'),
                  fit: BoxFit.cover,
                ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  Strings.choose_cloths,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: MyColors.blackColor,
                    fontSize: ScreenUtil().setSp(22),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(16),
                      fontWeight: FontWeight.normal,
                      color: MyColors.messageColor),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 30),
          child: Align(
            alignment: FractionalOffset.bottomCenter,
            child: InkWell(
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: MyColors.next_btn_color),
                child: Icon(
                  Icons.arrow_forward,
                  size: 30,
                  color: MyColors.whiteColor,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
