import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/ui/confirm_order_screen.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';

class PaymentMethodScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PaymentMethodScreenState();
  }
}

class PaymentMethodScreenState extends State<PaymentMethodScreen> {
  bool isTappedWallet = false;
  bool isTappedVisa = false;
  bool isTappedCashDelivery = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.unSelectedIndicatorColor,
      bottomNavigationBar: InkWell(
        onTap: () {
           Navigator.push(context,
            MaterialPageRoute(builder: (context)=>ConfirmOrderScreen()),);
        },
        child: Container(
            width: double.infinity,
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle, color: MyColors.next_btn_color),
            child: Text(
              Strings.confirm_order,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: MyColors.whiteColor,
                  fontSize: ScreenUtil().setSp(18)),
            )),
      ),
      appBar: AppBar(
          backgroundColor: MyColors.next_btn_color,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.whiteColor,
            ),
          ),
          title: Text(Strings.schedule_pickup),
          centerTitle: true),
      body: paymentMethodView(),
    );
  }

  // make view for select payment method
  Widget paymentMethodView() {
    return ListView(
      physics: BouncingScrollPhysics(),
      children: <Widget>[
        Container(
          width: double.infinity,
          padding: EdgeInsets.only(left: 40, right: 40, top: 8, bottom: 8),
          decoration: BoxDecoration(
            color: MyColors.whiteColor,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(image: AssetImage('assets/icons/location_blur.png')),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Location',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(10),
                        color: MyColors.blackColor.withOpacity(0.8),
                        fontWeight: FontWeight.w800),
                  )
                ],
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: Container(
                  height: 1,
                  color: MyColors.greyColor,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(
                    image: AssetImage('assets/icons/date_blur.png'),
                    height: 40,
                    width: 40,
                    fit: BoxFit.fill,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Date/Time',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(10),
                        color: MyColors.blackColor.withOpacity(0.8),
                        fontWeight: FontWeight.w800),
                  )
                ],
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: Container(
                  height: 1,
                  color: MyColors.greyColor,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(image: AssetImage('assets/icons/payment.png')),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Payment',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(10),
                        color: MyColors.blackColor.withOpacity(0.8),
                        fontWeight: FontWeight.w800),
                  )
                ],
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: Container(
                  height: 1,
                  color: MyColors.greyColor,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(image: AssetImage('assets/icons/complete_blur.png')),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Complete',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(10),
                        color: MyColors.blackColor.withOpacity(0.8),
                        fontWeight: FontWeight.w800),
                  )
                ],
              ),
            ],
          ),
        ),
        Container(
          width: double.infinity,
          height: 1.5,
          color: MyColors.greyColor.withOpacity(0.7),
        ),
        Container(
          margin: EdgeInsets.only(top: 15),
          decoration: BoxDecoration(color: MyColors.whiteColor),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(
                    left: 16, right: 16, top: 10, bottom: 10),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        setState(() {
                          isTappedCashDelivery = false;
                          isTappedVisa = false;
                          isTappedWallet = true;
                        });
                      },
                      child: Container(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: isTappedWallet
                                ? MyColors.next_btn_color
                                : MyColors.whiteColor,
                            border: Border.all(
                                color: isTappedWallet
                                    ? MyColors.next_btn_color
                                    : MyColors.blackColor,
                                width: 1.5)),
                        child: isTappedWallet
                            ? Icon(
                                Icons.check,
                                color: MyColors.whiteColor,
                                size: 20,
                              )
                            : null,
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Pay Via Wallet',
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(17),
                              fontWeight: FontWeight.w500,
                              color: MyColors.blackColor.withOpacity(0.8)),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          '123 4567 890',
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(14),
                              fontWeight: FontWeight.w500,
                              color: MyColors.greyColor),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                height: 1,
                color: MyColors.greyColor,
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 16, right: 16, top: 10, bottom: 10),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        setState(() {
                          isTappedWallet = false;
                          isTappedVisa = true;
                          isTappedCashDelivery = false;
                        });
                      },
                      child: Container(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: isTappedVisa
                                ? MyColors.next_btn_color
                                : MyColors.whiteColor,
                            border: Border.all(
                                color: isTappedVisa
                                    ? MyColors.next_btn_color
                                    : MyColors.blackColor,
                                width: 1.5)),
                        child: isTappedVisa
                            ? Icon(
                                Icons.check,
                                color: MyColors.whiteColor,
                                size: 20,
                              )
                            : null,
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Visa Debit Card',
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(17),
                              fontWeight: FontWeight.w500,
                              color: MyColors.blackColor.withOpacity(0.8)),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          '**** **** **** 4522',
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(14),
                              fontWeight: FontWeight.w500,
                              color: MyColors.greyColor),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                height: 1,
                color: MyColors.greyColor,
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 16, right: 16, top: 16, bottom: 16),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        setState(() {
                          isTappedCashDelivery = true;
                          isTappedVisa = false;
                          isTappedWallet = false;
                        });
                      },
                      child: Container(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: isTappedCashDelivery
                                ? MyColors.next_btn_color
                                : MyColors.whiteColor,
                            border: Border.all(
                                color: isTappedCashDelivery
                                    ? MyColors.next_btn_color
                                    : MyColors.blackColor,
                                width: 1.5)),
                        child: isTappedCashDelivery
                            ? Icon(
                                Icons.check,
                                size: 20,
                                color: MyColors.whiteColor,
                              )
                            : null,
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Cash On Delivery',
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(17),
                              fontWeight: FontWeight.w500,
                              color: MyColors.blackColor.withOpacity(0.8)),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          padding:
              const EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 10),
          margin: EdgeInsets.only(top: 20),
          decoration: BoxDecoration(color: MyColors.whiteColor),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Text(
                      'Pickup Address',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(18),
                          fontWeight: FontWeight.w500,
                          color: MyColors.blackColor.withOpacity(0.8)),
                    ),
                  ),
                  Icon(
                    Icons.edit,
                    color: MyColors.greyColor,
                    size: 20,
                  ),
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                'Home: - #2214, Phase IV,Mohali,Punjab, India, 160059',
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: MyColors.messageColor.withOpacity(0.5),
                    fontSize: ScreenUtil().setSp(13)),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 20),
          padding:
              const EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 10),
          decoration: BoxDecoration(color: MyColors.whiteColor),
          child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: Text(
                    'Pickup Date & Time',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(18),
                        fontWeight: FontWeight.w500,
                        color: MyColors.blackColor.withOpacity(0.8)),
                  ),
                ),
                Icon(
                  Icons.edit,
                  color: MyColors.greyColor,
                  size: 20,
                )
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              'Monday, 10 Nov 2020 between 10:00AM - 12:00 PM',
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  color: MyColors.messageColor.withOpacity(0.5),
                  fontSize: ScreenUtil().setSp(13)),
            ),
          ],
        ),
        ),
        Container(
          margin: EdgeInsets.only(top: 2),
          padding:
              const EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 10),
          decoration: BoxDecoration(color: MyColors.whiteColor),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Delivery Date & Time',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(18),
                    fontWeight: FontWeight.w500,
                    color: MyColors.blackColor.withOpacity(0.8)),
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                'Monday, 12 Nov 2020 between 10:00AM - 02:00 PM',
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: MyColors.messageColor.withOpacity(0.5),
                    fontSize: ScreenUtil().setSp(13)),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
