import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProfileScreenState();
  }
}

class ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
          width: double.infinity,
          color: MyColors.whiteColor,
          child: bottomView()),
      appBar: AppBar(
        backgroundColor: MyColors.next_btn_color,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back,
            color: MyColors.whiteColor,
          ),
        ),
        title: Text(Strings.profile),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.edit,
                color: MyColors.whiteColor,
              ),
              onPressed: () {})
        ],
      ),
      body: profileView(),
    );
  }

  // make view for profile screen
  Widget profileView() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/icons/splash.png'), fit: BoxFit.cover),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 20,bottom: 0,left: 30,right: 30),
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: <Widget>[
            Center(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: MyColors.whiteColor, width: 2),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0, 6),
                          blurRadius: 5,
                        ),
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        height: 150,
                        width: 150,
                        imageUrl:
                            'https://timesofindia.indiatimes.com/thumb/msid-72988628,imgsize-158263,width-400,resizemode-4/72988628.jpg',
                        placeholder: (context, url) =>
                            new CircularProgressIndicator(
                          strokeWidth: 1,
                        ),
                        errorWidget: (context, url, error) => new Icon(
                          Icons.error,
                          size: 60,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Card(
              elevation: 6,
              color: Colors.white.withOpacity(0.9),
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.white70, width: 1),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 20, bottom: 20),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      Strings.userName,
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(18),
                          fontWeight: FontWeight.bold,
                          color: MyColors.messageColor),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      'John Doe',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(16),
                          fontWeight: FontWeight.normal,
                          color: MyColors.messageColor),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      width: double.infinity,
                      height: 1,
                      color: MyColors.unSelectedIndicatorColor,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      Strings.phoneNumber,
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(18),
                          fontWeight: FontWeight.bold,
                          color: MyColors.messageColor),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      '+1 987 654 3210',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(16),
                          fontWeight: FontWeight.normal,
                          color: MyColors.messageColor),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      width: double.infinity,
                      height: 1,
                      color: MyColors.unSelectedIndicatorColor,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      Strings.emailId,
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(18),
                          fontWeight: FontWeight.bold,
                          color: MyColors.messageColor),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      'johndeo@gmail.com',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(16),
                          fontWeight: FontWeight.normal,
                          color: MyColors.messageColor),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      width: double.infinity,
                      height: 1,
                      color: MyColors.unSelectedIndicatorColor,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // make view for bottom widget
  Widget bottomView() {
    return Container(
      padding: EdgeInsets.all(16),
      margin: EdgeInsets.only(bottom:30),
      decoration:
          BoxDecoration(shape: BoxShape.circle, color: MyColors.next_btn_color),
      child: Icon(
        Icons.power_settings_new,
        size: 30,
        color: MyColors.whiteColor,
      ),
    );
  }
}
