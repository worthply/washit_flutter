import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/tab_view_pages/MensView.dart';
import 'package:washit/tab_view_pages/beddingView.dart';
import 'package:washit/tab_view_pages/house_hold_view.dart';
import 'package:washit/tab_view_pages/womens_view.dart';
import 'package:washit/utils/my_colors.dart';

class WashAndFoldScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return WashAndFoldScreenState();
  }
}

class WashAndFoldScreenState extends State<WashAndFoldScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  final List<Tab> tabs = <Tab>[
    Tab(text: "Men's"),
    Tab(text: "Women's"),
    Tab(text: "Bedding"),
    Tab(text: "Household")
  ];

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: tabs.length);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColors.next_btn_color,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.whiteColor,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text(
          'Wash & Fold',
          style: TextStyle(
              fontSize: ScreenUtil().setSp(18),
              fontWeight: FontWeight.bold,
              color: MyColors.whiteColor),
        ),
        centerTitle: true,
        elevation: 0,
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.search,
              color: MyColors.whiteColor,
            ),
          )
        ],
      ),
      body: tabViewScreen(),
    );
  }

  // make view for different tab
  Widget tabViewScreen() {
    return DefaultTabController(
      length: 4,
      child: Column(
        children: <Widget>[
          TabBar(
            isScrollable: false,
            unselectedLabelColor: MyColors.greyColor,
            labelColor: MyColors.whiteColor,
            unselectedLabelStyle: TextStyle(color: MyColors.greyColor),
            indicatorSize: TabBarIndicatorSize.tab,
            indicator: BubbleTabIndicator(
              indicatorHeight: 30.0,
              indicatorColor: MyColors.next_btn_color,
              tabBarIndicatorSize: TabBarIndicatorSize.tab,
            ),
            tabs: tabs,
            controller: _tabController,
          ),
          Container(
            width: double.infinity,
            height: 1,
            color: MyColors.greyColor,
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: <Widget>[
                MensView(),
                WomensView(),
                BeddingView(),
                HouseHoldView(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
