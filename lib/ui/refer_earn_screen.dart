import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';

class ReferEarnScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ReferEarnScreenState();
  }
}

class ReferEarnScreenState extends State<ReferEarnScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
          width: double.infinity,
          color: MyColors.whiteColor,
          child: bottomView()),
      backgroundColor: MyColors.unSelectedIndicatorColor,
      appBar: AppBar(
          backgroundColor: MyColors.next_btn_color,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.whiteColor,
            ),
          ),
          title: Text(Strings.rafer_and_earn),
          centerTitle: true),
      body: referEarnView(),
    );
  }

  // make view for bottom widget
  Widget bottomView() {
    return Container(
      padding: EdgeInsets.all(16),
      margin: EdgeInsets.only(bottom: 30),
      decoration:
          BoxDecoration(shape: BoxShape.circle, color: MyColors.next_btn_color),
      child: Icon(
        Icons.share,
        size: 30,
        color: MyColors.whiteColor,
      ),
    );
  }

  // make view for refer and earn
  Widget referEarnView() {
    return Container(
        width: double.infinity,
        height: double.infinity,
        color: MyColors.whiteColor,
        child: Center(
          child: ListView(
            padding: EdgeInsets.only(left: 40, right: 40),
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            children: <Widget>[
              Image(
                image: AssetImage('assets/icons/refer.jpg'),
                fit: BoxFit.cover,
              ),
              Center(
                  child: Text(
                Strings.refer_and_earn_free_wash,
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(27),
                    fontWeight: FontWeight.bold,
                    color: MyColors.messageColor),
              )),
              SizedBox(
                height: 5,
              ),
              Center(
                  child: Text(
                'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(14),
                    fontWeight: FontWeight.normal,
                    color: MyColors.greyColor),
              )),
              SizedBox(
                height: 15,
              ),
              Center(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    DottedBorder(
                      color: MyColors.greyColor,
                      borderType: BorderType.RRect,
                      radius: Radius.circular(8),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        child: Container(
                          height: 50,
                          width: 150,
                          color: MyColors.referAndContainerColor,
                          child: Center(
                            child: Text(
                              'ABC12345',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: ScreenUtil().setSp(20),
                                  color: MyColors.blackColor,
                                  letterSpacing: 2),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
