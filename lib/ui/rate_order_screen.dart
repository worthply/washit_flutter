import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';

class RateOrderScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RateOrderScreenState();
  }
}

class RateOrderScreenState extends State<RateOrderScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
          width: double.infinity,
          color: MyColors.whiteColor,
          child: bottomView()),
      appBar: AppBar(
          backgroundColor: MyColors.next_btn_color,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.whiteColor,
            ),
          ),
          title: Text(Strings.rateOrder),
          centerTitle: true),
      body: referEarnView(),
    );
  }

  // make view for bottom widget
  Widget bottomView() {
    return Container(
        width: double.infinity,
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle, color: MyColors.next_btn_color),
        child: Text(
          Strings.submit,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              color: MyColors.whiteColor,
              fontSize: ScreenUtil().setSp(18)),
        ));
  }

  // make view for rate order
  Widget referEarnView() {
    return Container(
      child: ListView(
        padding: EdgeInsets.only(top: 60, bottom: 30, left: 30, right: 30),
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Center(
            child: Image(
              image: AssetImage('assets/icons/success_order.png'),
              fit: BoxFit.fill,
              height: 150,
              width: 150,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Your order has been \nsuccessfully delivered.',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(20),
                fontWeight: FontWeight.w500,
                color: MyColors.messageColor),
          ),
          SizedBox(
            height: 80,
          ),
          Text(
            'Please rate your last order experience.',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(18),
                fontWeight: FontWeight.w400,
                color: MyColors.messageColor),
          ),
          SizedBox(
            height: 10,
          ),
          Center(
            child: RatingBar(
              unratedColor: MyColors.greyColor,
              initialRating: 0,
              minRating: 0,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemPadding: EdgeInsets.symmetric(horizontal: 6.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
              ),
              onRatingUpdate: (rating) {
                print(rating);
              },
            ),
          ),
          SizedBox(
            height: 30,
          ),
          TextField(
            decoration: InputDecoration(
                hintText: 'Say something about your experience.',
                alignLabelWithHint: true,
                hintStyle: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: MyColors.greyColor,
                    fontSize: ScreenUtil().setSp(20))),
          )
        ],
      ),
    );
  }
}
