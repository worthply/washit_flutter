import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/ui/payment_method_screen.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';

class DateTimeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return DateTimeScreenState();
  }
}

class DateTimeScreenState extends State<DateTimeScreen> {
  List<String> pickUpTimeList = List();
  bool isTimePickUp = false;
  bool deliveryTime = false;
  int position = -1;
  int deliveryIndexPosition = -1;
  int month = DateTime.now().month;
  int deliveryMonth = DateTime.now().month;
  int year = DateTime.now().year;
  int deliveryYear = DateTime.now().year;
  bool isDaySelected = false;
  bool isDeliveryDaySelected = false;
  int tappedPosition = -1;
  int tappedPositionDelivery = -1;

  @override
  void initState() {
    super.initState();

    pickUpTimeList.add('08:00 AM - 10:00 AM');
    pickUpTimeList.add('10:00 AM - 12:00 PM');
    pickUpTimeList.add('12:00 PM - 02:00 PM');
    pickUpTimeList.add('02:00 PM - 04:00 PM');
    pickUpTimeList.add('04:00 PM - 06:00 PM');
    pickUpTimeList.add('06:00 PM - 08:00 PM');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.unSelectedIndicatorColor,
      bottomNavigationBar: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => PaymentMethodScreen()),
          );
        },
        child: Container(
            width: double.infinity,
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle, color: MyColors.next_btn_color),
            child: Text(
              Strings.continuePaymentMethod,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: MyColors.whiteColor,
                  fontSize: ScreenUtil().setSp(18)),
            )),
      ),
      appBar: AppBar(
          backgroundColor: MyColors.next_btn_color,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.whiteColor,
            ),
          ),
          title: Text(Strings.schedule_pickup),
          centerTitle: true),
      body: dateTimeSelectView(),
    );
  }

  // make view for date time select
  Widget dateTimeSelectView() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: double.infinity,
          padding: EdgeInsets.only(left: 40, right: 40, top: 8, bottom: 8),
          decoration: BoxDecoration(
              color: MyColors.whiteColor,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(8),
                bottomRight: Radius.circular(8),
              )),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(image: AssetImage('assets/icons/location_blur.png')),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Location',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(10),
                        color: MyColors.blackColor.withOpacity(0.8),
                        fontWeight: FontWeight.w800),
                  )
                ],
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: Container(
                  height: 1,
                  color: MyColors.greyColor,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(
                    image: AssetImage('assets/icons/date.png'),
                    height: 40,
                    width: 40,
                    fit: BoxFit.fill,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Date/Time',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(10),
                        color: MyColors.blackColor.withOpacity(0.8),
                        fontWeight: FontWeight.w800),
                  )
                ],
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: Container(
                  height: 1,
                  color: MyColors.greyColor,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(image: AssetImage('assets/icons/payment_blur.png')),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Payment',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(10),
                        color: MyColors.blackColor.withOpacity(0.8),
                        fontWeight: FontWeight.w800),
                  )
                ],
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: Container(
                  height: 1,
                  color: MyColors.greyColor,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(image: AssetImage('assets/icons/complete_blur.png')),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Complete',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(10),
                        color: MyColors.blackColor.withOpacity(0.8),
                        fontWeight: FontWeight.w800),
                  )
                ],
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(16),
          margin: EdgeInsets.only(top: 8),
          width: double.infinity,
          decoration: BoxDecoration(color: MyColors.whiteColor),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Text(
                      'Pickup Date',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(16),
                          fontWeight: FontWeight.bold,
                          color: MyColors.next_btn_color),
                    ),
                  ),
                  InkWell(
                    onTap: () {},
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 18,
                      color: MyColors.greyColor,
                    ),
                  ),
                  Text(
                    getMonthYear(0),
                    style: TextStyle(
                        fontSize: 14,
                        color: MyColors.blackColor,
                        fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    onTap: () {
                      DateTime currentDateTime = DateTime.now();
                      print('Time $currentDateTime');
                    },
                    child: Icon(
                      Icons.arrow_forward_ios,
                      size: 18,
                      color: MyColors.greyColor,
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              DatePickerTimeline(
                /*DateTime.now(),*/
                currentDate: DateTime.now(),
                width: double.infinity,
                selectionColor:
                    MyColors.unSelectedIndicatorColor.withOpacity(0.1),
                onDateChange: (date, index) {
                  // New date selected
                  print(date.day.toString());
                  month = date.month;
                  year = date.year;
                  isDeliveryDaySelected = true;
                  tappedPositionDelivery = index;
                  setState(() {});
                },
                position: tappedPositionDelivery,
                isDaySelected: isDeliveryDaySelected,
              ),
            ],
          ),
        ),
        Container(
          width: double.infinity,
          height: .3,
          color: MyColors.greyColor.withOpacity(0.7),
        ),
        Container(
          height: 100,
          padding: EdgeInsets.all(16),
          width: double.infinity,
          decoration: BoxDecoration(color: MyColors.whiteColor),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Pickup Time Slot',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(16),
                    fontWeight: FontWeight.bold,
                    color: MyColors.next_btn_color),
              ),
              SizedBox(
                height: 10,
              ),
              Expanded(
                child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: 6,
                    itemBuilder: (BuildContext context, int index) =>
                        pickUpTimeRow(index, pickUpTimeList)),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(16),
          margin: EdgeInsets.only(top: 8),
          width: double.infinity,
          decoration: BoxDecoration(color: MyColors.whiteColor),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Text(
                      'Delivery Date',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(16),
                          fontWeight: FontWeight.bold,
                          color: MyColors.next_btn_color),
                    ),
                  ),
                  InkWell(
                    onTap: () {},
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 18,
                      color: MyColors.greyColor,
                    ),
                  ),
                  Text(
                    getMonthYear(1),
                    style: TextStyle(
                        fontSize: 14,
                        color: MyColors.blackColor,
                        fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    onTap: () {
                      DateTime currentDateTime = DateTime.now();
                      print('Time $currentDateTime');
                    },
                    child: Icon(
                      Icons.arrow_forward_ios,
                      size: 18,
                      color: MyColors.greyColor,
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              DatePickerTimeline(
                /*DateTime.now(),*/
                currentDate: DateTime.now(),
                width: double.infinity,
                selectionColor:
                    MyColors.unSelectedIndicatorColor.withOpacity(0.1),
                onDateChange: (date, index) {
                  // New date selected
                  print(date.day.toString());
                  deliveryMonth = date.month;
                  deliveryYear = date.year;
                  isDaySelected = true;
                  tappedPosition = index;
                  setState(() {});
                },
                position: tappedPosition,
                isDaySelected: isDaySelected,
              ),
            ],
          ),
        ),
        Container(
          width: double.infinity,
          height: .3,
          color: MyColors.greyColor.withOpacity(0.7),
        ),
        Container(
          height: 100,
          padding: EdgeInsets.all(16),
          width: double.infinity,
          decoration: BoxDecoration(color: MyColors.whiteColor),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Delivery Time Slot',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(16),
                    fontWeight: FontWeight.bold,
                    color: MyColors.next_btn_color),
              ),
              SizedBox(
                height: 10,
              ),
              Expanded(
                child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: 6,
                    itemBuilder: (BuildContext context, int index) =>
                        deliveryTimeRow(index, pickUpTimeList)),
              ),
            ],
          ),
        ),
      ],
    );
  }

  // make row for pickup time
  Widget pickUpTimeRow(int index, List<String> itemList) {
    return InkWell(
      onTap: () {
        setState(() {
          isTimePickUp = true;
          position = index;
        });
      },
      child: Container(
        margin:
            index == 0 ? EdgeInsets.only(left: 0) : EdgeInsets.only(left: 8),
        padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
        decoration: BoxDecoration(
            color: MyColors.unSelectedIndicatorColor.withOpacity(0.3),
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
                color: position == index
                    ? MyColors.itemPriceColor
                    : MyColors.unSelectedIndicatorColor.withOpacity(0.5),
                width: 1.5)),
        child: Text(
          itemList[index],
          style: TextStyle(
              fontWeight: FontWeight.bold,
              color: MyColors.blackColor.withOpacity(0.7),
              fontSize: ScreenUtil().setSp(16)),
        ),
      ),
    );
  }

  // make row for delivery time
  Widget deliveryTimeRow(int index, List<String> itemList) {
    return InkWell(
      onTap: () {
        setState(() {
          deliveryTime = true;
          deliveryIndexPosition = index;
        });
      },
      child: Container(
        margin:
            index == 0 ? EdgeInsets.only(left: 0) : EdgeInsets.only(left: 8),
        padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
        decoration: BoxDecoration(
            color: MyColors.unSelectedIndicatorColor.withOpacity(0.3),
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
                color: deliveryIndexPosition == index
                    ? MyColors.itemPriceColor
                    : MyColors.unSelectedIndicatorColor.withOpacity(0.5),
                width: 1.5)),
        child: Text(
          itemList[index],
          style: TextStyle(
              fontWeight: FontWeight.bold,
              color: MyColors.blackColor.withOpacity(0.7),
              fontSize: ScreenUtil().setSp(16)),
        ),
      ),
    );
  }

  // get Current month year
  getMonthYear(int value) {
    if (value == 1) {
      return deliveryMonth == 1
          ? "January $deliveryYear"
          : deliveryMonth == 2
              ? "February $deliveryYear"
              : deliveryMonth == 3
                  ? "March $deliveryYear"
                  : deliveryMonth == 4
                      ? "April $deliveryYear"
                      : deliveryMonth == 5
                          ? "May $deliveryYear"
                          : deliveryMonth == 6
                              ? "June $deliveryYear"
                              : deliveryMonth == 7
                                  ? "July $deliveryYear"
                                  : deliveryMonth == 8
                                      ? "August $deliveryYear"
                                      : deliveryMonth == 9
                                          ? "September $deliveryYear"
                                          : deliveryMonth == 10
                                              ? "October $deliveryYear"
                                              : deliveryMonth == 11
                                                  ? "November $deliveryYear"
                                                  : "December $deliveryYear";
    } else {
      return month == 1
          ? "January $year"
          : month == 2
              ? "February $year"
              : month == 3
                  ? "March $year"
                  : month == 4
                      ? "April $year"
                      : month == 5
                          ? "May $year"
                          : month == 6
                              ? "June $year"
                              : month == 7
                                  ? "July $year"
                                  : month == 8
                                      ? "August $year"
                                      : month == 9
                                          ? "September $year"
                                          : month == 10
                                              ? "October $year"
                                              : month == 11
                                                  ? "November $year"
                                                  : "December $year";
    }
  }
}
