import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:washit/ui/about_us_screen.dart';
import 'package:washit/ui/all_orders_screen.dart';
import 'package:washit/ui/notification_screen.dart';
import 'package:washit/ui/offersScreen.dart';
import 'package:washit/ui/profile_screen.dart';
import 'package:washit/ui/rate_order_screen.dart';
import 'package:washit/ui/refer_earn_screen.dart';
import 'package:washit/ui/washAndFoldScreen.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';
import 'package:washit/widget/circular_clipper.dart';
import 'package:washit/widget/drawer_view.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  PageController pageController = PageController();
  bool isTapped = false;
  bool isTappedNotification = false;
  bool isTappedAboutUs = false;
  bool isTappedAllOrders = false;
  bool isTappedRefer = false;
  bool isTappedSettings = false;
  bool isTappedHome = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerView(),
      key: scaffoldKey,
      appBar: isTapped
          ? AppBar(
              backgroundColor: MyColors.next_btn_color,
              leading: IconButton(
                onPressed: () {
                  setState(() {
                    isTapped = false;
                    isTappedNotification = false;
                    isTappedAboutUs = false;
                    isTappedAllOrders = false;
                    isTappedRefer = false;
                    isTappedSettings = false;
                    isTappedHome = true;
                  });
                },
                icon: Icon(
                  Icons.close,
                  color: MyColors.whiteColor,
                ),
              ),
              title: Text(Strings.menu),
              centerTitle: true)
          : AppBar(
              backgroundColor: MyColors.next_btn_color,
              leading: IconButton(
                onPressed: () {
                  setState(() {
                    isTapped = true;
                  });
                  //return scaffoldKey.currentState.openDrawer();
                },
                icon: Icon(
                  Icons.menu,
                  color: MyColors.whiteColor,
                ),
              ),
              title: Text(Strings.home),
              centerTitle: true,
              actions: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ProfileScreen()),
                    );
                  },
                  child: Center(
                    child: Container(
                      width: 35,
                      height: 35,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border:
                              Border.all(color: MyColors.whiteColor, width: 1),
                          image: DecorationImage(
                            image: AssetImage('assets/icons/rohit.jpeg'),
                            fit: BoxFit.cover,
                          )),
                    ),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
              ],
            ),
      body: homeView(),
    );
  }

  // make view for home screen
  Widget homeView() {
    return Stack(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          // shrinkWrap: true,
          children: <Widget>[
            Container(
              height: 200,
              width: double.infinity,
              child: PageIndicatorContainer(
                align: IndicatorAlign.bottom,
                length: 5,
                indicatorSpace: 7,
                indicatorColor: MyColors.unSelectedIndicatorColor,
                indicatorSelectorColor: MyColors.whiteColor,
                shape: IndicatorShape.circle(size: 6),
                child: PageView.builder(
                  itemBuilder: (context, index) {
                    return pageViewItem(index);
                  },
                  itemCount: 5,
                  controller: pageController,
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Center(
              child: Text(
                Strings.services,
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(18),
                    fontWeight: FontWeight.bold,
                    color: MyColors.next_btn_color),
              ),
            ),
            Expanded(
              child: ListView.builder(
                padding: EdgeInsets.all(8),
                itemBuilder: (context, index) {
                  return servicesItemRow(index);
                },
                itemCount: 3,
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
              ),
            )
          ],
        ),
        isTapped ? drawerView() : Container(),
      ],
    );
  }

  // make row for page view item
  Widget pageViewItem(int index) {
    return Container(
      height: 150,
      width: 150,
      child: Stack(
        children: <Widget>[
          ClipShadowPath(
            clipper: CircularClipper(),
            shadow: Shadow(blurRadius: 1),
            child: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                          "https://content.jdmagicbox.com/comp/ernakulam/l9/0484px484.x484.180909170512.j1l9/catalogue/grooms-wedding-hub-perumbavoor-ernakulam-costumes-on-hire-o4cbd2w1zk.jpg"))),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 60),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.only(top: 16, bottom: 16),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.black54,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "Flat 10% off on Dry Clean of Coats & Blazers",
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // make row for services item
  Widget servicesItemRow(int index) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  index == 1 ? OffersScreen() : WashAndFoldScreen()),
        );
      },
      child: Card(
        color: MyColors.whiteColor,
        elevation: 4,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Padding(
          padding:
              const EdgeInsets.only(left: 30, right: 30, top: 16, bottom: 16),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Text(
                    'Wash & Fold',
                    style: TextStyle(
                        fontWeight: FontWeight.normal,
                        color: MyColors.blackColor,
                        fontSize: ScreenUtil().setSp(20)),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                          left: 12, right: 12, top: 5, bottom: 5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          shape: BoxShape.rectangle,
                          color: MyColors.blackColor),
                      child: Text(
                        '48 Hours',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(16),
                            fontWeight: FontWeight.normal,
                            color: MyColors.whiteColor),
                      )),
                ],
              ),
              Image(
                image: AssetImage('assets/icons/shirt.jpeg'),
                fit: BoxFit.cover,
                height: 100,
              )
            ],
          ),
        ),
      ),
    );
  }

  // make view for drawer layout
  Widget drawerView() {
    return Container(
      height: 250,
      width: double.infinity,
      padding: EdgeInsets.only(left: 5, right: 5, top: 16, bottom: 16),
      decoration: BoxDecoration(
        color: MyColors.blackColor.withOpacity(0.8),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      isTappedNotification = false;
                      isTappedAboutUs = false;
                      isTappedAllOrders = false;
                      isTappedRefer = false;
                      isTappedSettings = false;
                      isTappedHome = true;
                    });
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: isTappedHome
                                ? MyColors.next_btn_color
                                : MyColors.whiteColor),
                        child: Icon(
                          Icons.home,
                          size: 30,
                          color: isTappedHome
                              ? MyColors.whiteColor
                              : MyColors.next_btn_color,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        Strings.home,
                        style: TextStyle(
                            color: isTappedHome
                                ? MyColors.next_btn_color
                                : MyColors.whiteColor,
                            fontWeight: FontWeight.bold,
                            fontSize: ScreenUtil().setSp(16)),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      isTappedNotification = true;
                      isTappedAboutUs = false;
                      isTappedAllOrders = false;
                      isTappedRefer = false;
                      isTappedSettings = false;
                      isTappedHome = false;
                    });
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => NotificationScreen()),
                    );
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: isTappedNotification
                                ? MyColors.next_btn_color
                                : MyColors.whiteColor),
                        child: Icon(
                          Icons.notifications,
                          size: 30,
                          color: isTappedNotification
                              ? MyColors.whiteColor
                              : MyColors.next_btn_color,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        Strings.notification,
                        style: TextStyle(
                            color: isTappedNotification
                                ? MyColors.next_btn_color
                                : MyColors.whiteColor,
                            fontWeight: FontWeight.bold,
                            fontSize: ScreenUtil().setSp(16)),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      isTappedAboutUs = true;
                      isTappedNotification = false;
                      isTappedAllOrders = false;
                      isTappedRefer = false;
                      isTappedSettings = false;
                      isTappedHome = false;
                    });
                    Navigator.push(context,
                    MaterialPageRoute(builder: (context)=>AboutUsScreen()),);
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.all(16),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: isTappedAboutUs
                                  ? MyColors.next_btn_color
                                  : MyColors.whiteColor),
                          child: Image(
                            image: AssetImage('assets/icons/splash_icon.png'),
                            fit: BoxFit.fill,
                            height: 32,
                            width: 32,
                          )),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        Strings.about_us,
                        style: TextStyle(
                            color: isTappedAboutUs
                                ? MyColors.next_btn_color
                                : MyColors.whiteColor,
                            fontWeight: FontWeight.bold,
                            fontSize: ScreenUtil().setSp(16)),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      isTappedAllOrders = true;
                      isTappedAboutUs = false;
                      isTappedNotification = false;
                      isTappedRefer = false;
                      isTappedSettings = false;
                      isTappedHome = false;
                    });

                    Navigator.push(context,
                    MaterialPageRoute(builder: (context)=>AllOrderScreen()),);
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: isTappedAllOrders
                                ? MyColors.next_btn_color
                                : MyColors.whiteColor),
                        child: Icon(
                          Icons.list,
                          size: 30,
                          color: isTappedAllOrders
                              ? MyColors.whiteColor
                              : MyColors.next_btn_color,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        Strings.all_orders,
                        style: TextStyle(
                            color: isTappedAllOrders
                                ? MyColors.next_btn_color
                                : MyColors.whiteColor,
                            fontWeight: FontWeight.bold,
                            fontSize: ScreenUtil().setSp(16)),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      isTappedAllOrders = false;
                      isTappedAboutUs = false;
                      isTappedNotification = false;
                      isTappedRefer = true;
                      isTappedSettings = false;
                      isTappedHome = false;
                    });
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ReferEarnScreen()),
                    );
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: isTappedRefer
                                ? MyColors.next_btn_color
                                : MyColors.whiteColor),
                        child: Icon(
                          Icons.share,
                          size: 30,
                          color: isTappedRefer
                              ? MyColors.whiteColor
                              : MyColors.next_btn_color,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        Strings.rafer_and_earn,
                        style: TextStyle(
                            color: isTappedRefer
                                ? MyColors.next_btn_color
                                : MyColors.whiteColor,
                            fontWeight: FontWeight.bold,
                            fontSize: ScreenUtil().setSp(16)),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      isTappedAllOrders = false;
                      isTappedAboutUs = false;
                      isTappedNotification = false;
                      isTappedRefer = false;
                      isTappedSettings = true;
                      isTappedHome = false;
                    });

                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => RateOrderScreen()),
                    );
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.all(16),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: isTappedSettings
                                  ? MyColors.next_btn_color
                                  : MyColors.whiteColor),
                          child: Icon(
                            Icons.settings,
                            size: 30,
                            color: isTappedSettings
                                ? MyColors.whiteColor
                                : MyColors.next_btn_color,
                          )),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        Strings.settings,
                        style: TextStyle(
                            color: isTappedSettings
                                ? MyColors.next_btn_color
                                : MyColors.whiteColor,
                            fontWeight: FontWeight.bold,
                            fontSize: ScreenUtil().setSp(16)),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
