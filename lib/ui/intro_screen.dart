import 'package:flutter/material.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:washit/utils/my_colors.dart';
import 'package:washit/view_pager_pages/fifth_page.dart';
import 'package:washit/view_pager_pages/first_page.dart';
import 'package:washit/view_pager_pages/fourth_page.dart';
import 'package:washit/view_pager_pages/second_page.dart';
import 'package:washit/view_pager_pages/third_page.dart';

class IntroScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return IntroScreenState();
  }
}

class IntroScreenState extends State<IntroScreen> {
  PageController pageController = PageController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: introView(),
    );
  }

  // make view for intro screen
  Widget introView() {
    return PageIndicatorContainer(
      align: IndicatorAlign.bottom,
      length: 5,
      indicatorSpace: 12,
      padding: EdgeInsets.only(bottom: 120),
      indicatorColor: MyColors.unSelectedIndicatorColor,
      indicatorSelectorColor: MyColors.next_btn_color,
      shape: IndicatorShape.circle(size: 10),
      child: PageView(
        controller: pageController,
        scrollDirection: Axis.horizontal,
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          FirstPage(pageController),
          SecondPage(),
          ThirdPage(),
          FourthPage(),
          FifthPage(),
        ],
      ),
    );
  }
}