import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';

class ConfirmOrderScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ConfirmOrderScreenState();
  }
}

class ConfirmOrderScreenState extends State<ConfirmOrderScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: MyColors.unSelectedIndicatorColor,
      bottomNavigationBar: InkWell(
        onTap: () {
          /*Navigator.push(context,
            MaterialPageRoute(builder: (context)=>ConfirmOrderScreen()),);*/
        },
        child: Container(
            width: double.infinity,
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle, color: MyColors.next_btn_color),
            child: Text(
              Strings.orderStatus,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: MyColors.whiteColor,
                  fontSize: ScreenUtil().setSp(18)),
            )),
      ),
      appBar: AppBar(
          backgroundColor: MyColors.next_btn_color,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.whiteColor,
            ),
          ),
          title: Text(Strings.schedule_pickup),
          centerTitle: true),
      body: confirmOrderView(),
    );
  }

  // make view for confirm Order
  Widget confirmOrderView() {
    return Container(
      color: MyColors.unSelectedIndicatorColor.withOpacity(0.3),
      child: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.only(left: 40, right: 40, top: 8, bottom: 8),
            decoration: BoxDecoration(
              color: MyColors.whiteColor,
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image(image: AssetImage('assets/icons/location_blur.png')),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Location',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(10),
                          color: MyColors.blackColor.withOpacity(0.8),
                          fontWeight: FontWeight.w800),
                    )
                  ],
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Container(
                    height: 1,
                    color: MyColors.greyColor,
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image(
                      image: AssetImage('assets/icons/date_blur.png'),
                      height: 40,
                      width: 40,
                      fit: BoxFit.fill,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Date/Time',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(10),
                          color: MyColors.blackColor.withOpacity(0.8),
                          fontWeight: FontWeight.w800),
                    )
                  ],
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Container(
                    height: 1,
                    color: MyColors.greyColor,
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image(image: AssetImage('assets/icons/payment_blur.png')),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Payment',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(10),
                          color: MyColors.blackColor.withOpacity(0.8),
                          fontWeight: FontWeight.w800),
                    )
                  ],
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Container(
                    height: 1,
                    color: MyColors.greyColor,
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image(image: AssetImage('assets/icons/complete.png')),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Complete',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(10),
                          color: MyColors.blackColor.withOpacity(0.8),
                          fontWeight: FontWeight.w800),
                    )
                  ],
                ),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            height: 1.5,
            color: MyColors.greyColor.withOpacity(0.7),
          ),
          SizedBox(
            height: 30,
          ),
          Center(
              child: Image(
            image: AssetImage('assets/icons/intro_second.png'),
            fit: BoxFit.fill,
          )),
          SizedBox(
            height: 10,
          ),
          Text(
            'Thank You For Choosing Us!',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(22),
                color: MyColors.blackColor.withOpacity(0.8),
                fontWeight: FontWeight.w800),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            'Your pickup has been confirmed.',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(16),
                color: MyColors.blackColor,
                fontWeight: FontWeight.w400),
          ),
          SizedBox(
            height: 10,
          ),
          Card(
            margin: EdgeInsets.all(16),
            elevation: 4,
            color: MyColors.whiteColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          'Order Id',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: MyColors.blackColor,
                              fontSize: ScreenUtil().setSp(16)),
                        ),
                      ),
                      Text(
                        '2017121085',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            color: MyColors.blackColor,
                            fontSize: ScreenUtil().setSp(16)),
                      )
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 1,
                  color: MyColors.greyColor,
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          'Final Amount',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: MyColors.blackColor,
                              fontSize: ScreenUtil().setSp(16)),
                        ),
                      ),
                      Text(
                        '21.75\$',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            color: MyColors.blackColor,
                            fontSize: ScreenUtil().setSp(16)),
                      )
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 1,
                  color: MyColors.greyColor,
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Pickup Date & Time',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(16),
                            fontWeight: FontWeight.w500,
                            color: MyColors.blackColor.withOpacity(0.8)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Monday, 10 Nov 2020 between 10:00AM - 12:00 PM',
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: MyColors.messageColor.withOpacity(0.5),
                            fontSize: ScreenUtil().setSp(13)),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 1,
                  color: MyColors.greyColor,
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          'Payment Method',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: MyColors.blackColor,
                              fontSize: ScreenUtil().setSp(16)),
                        ),
                      ),
                      Text(
                        'XXXX-4143',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            color: MyColors.blackColor,
                            fontSize: ScreenUtil().setSp(16)),
                      )
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
