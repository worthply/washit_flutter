import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';

class OrderDetailsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return OrderDetailsScreenState();
  }
}

class OrderDetailsScreenState extends State<OrderDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.unSelectedIndicatorColor,
      appBar: AppBar(
          backgroundColor: MyColors.next_btn_color,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.whiteColor,
            ),
          ),
          title: Text(Strings.orderDetails),
          centerTitle: true),
      body: orderDetailsView(),
    );
  }

  // make view for order details
  Widget orderDetailsView() {
    return ListView(
      physics: BouncingScrollPhysics(),
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 7, bottom: 6),
          width: double.infinity,
          color: MyColors.whiteColor,
          child: Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text(
                      'Order No - 110040717',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(18),
                          fontWeight: FontWeight.w600,
                          color: MyColors.blackColor),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Order delivered successfully',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: MyColors.orderSuccessColor,
                          fontSize: ScreenUtil().setSp(16)),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Time: 11:35 AM, 15 June 2020',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: MyColors.messageColor.withOpacity(0.5),
                          fontSize: ScreenUtil().setSp(13)),
                    ),
                  ],
                )),
                Text(
                  '\$474',
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(16),
                      color: MyColors.blackColor,
                      fontWeight: FontWeight.w600),
                )
              ],
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 6),
          width: double.infinity,
          color: MyColors.whiteColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(
                    left: 16, right: 16, top: 10, bottom: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Pickup Address',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(18),
                          fontWeight: FontWeight.w500,
                          color: MyColors.blackColor.withOpacity(0.8)),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Home: - #2214, Phase IV,Mohali,Punjab, India, 160059',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: MyColors.messageColor.withOpacity(0.5),
                          fontSize: ScreenUtil().setSp(13)),
                    ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                color: MyColors.greyColor,
                height: 0.3,
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 16, right: 16, top: 10, bottom: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Pickup Date & Time',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(18),
                          fontWeight: FontWeight.w500,
                          color: MyColors.blackColor.withOpacity(0.8)),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Monday, 10 Nov 2020 between 10:00AM - 12:00 PM',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: MyColors.messageColor.withOpacity(0.5),
                          fontSize: ScreenUtil().setSp(13)),
                    ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                color: MyColors.greyColor,
                height: 0.3,
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 16, right: 16, top: 10, bottom: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Delivery Date & Time',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(18),
                          fontWeight: FontWeight.w500,
                          color: MyColors.blackColor.withOpacity(0.8)),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Monday, 12 Nov 2020 between 10:00AM - 02:00 PM',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: MyColors.messageColor.withOpacity(0.5),
                          fontSize: ScreenUtil().setSp(13)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 6),
          padding: EdgeInsets.only(bottom: 10),
          width: double.infinity,
          color: MyColors.whiteColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(
                    top: 16, bottom: 10, left: 16, right: 16),
                child: Text(
                  'Washup & Fold',
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(20),
                      fontWeight: FontWeight.w700,
                      color: MyColors.blackColor.withOpacity(0.8)),
                ),
              ),
              Container(
                width: double.infinity,
                color: MyColors.blackColor,
                height: 0.7,
              ),
              ListView.builder(
                  itemCount: 6,
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return clothesItemRow(index);
                  }),
            ],
          ),
        ),
        Container(
          width: double.infinity,
          color: MyColors.whiteColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(
                    left: 16, right: 16, top: 10, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        'Subtotal',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(16),
                            fontWeight: FontWeight.w400,
                            color: MyColors.blackColor),
                      ),
                    ),
                    Text(
                      '\$220.50',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: MyColors.messageColor,
                          fontSize: ScreenUtil().setSp(16)),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16,right: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        'Tax',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(16),
                            fontWeight: FontWeight.w400,
                            color: MyColors.blackColor),
                      ),
                    ),
                    Text(
                      '\$20.25',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: MyColors.messageColor,
                          fontSize: ScreenUtil().setSp(16)),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16,right: 16,top: 10,bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        'Total',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(16),
                            fontWeight: FontWeight.w700,
                            color: MyColors.blackColor),
                      ),
                    ),
                    Text(
                      '\$240.50',
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: MyColors.messageColor,
                          fontSize: ScreenUtil().setSp(16)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  // make row for clothes item
  Widget clothesItemRow(int index) {
    return Padding(
      padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom:5),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
              child: Text(
            '2 X T-Shirt - (Men)',
            style: TextStyle(
                fontWeight: FontWeight.normal,
                color: MyColors.blackColor.withOpacity(0.8),
                fontSize: ScreenUtil().setSp(18)),
          )),
          Text(
            '\$10.00',
            style: TextStyle(
                fontWeight: FontWeight.normal,
                color: MyColors.blackColor.withOpacity(0.8),
                fontSize: ScreenUtil().setSp(18)),
          )
        ],
      ),
    );
  }
}
