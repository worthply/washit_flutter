import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';

class OffersScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return OffersScreenState();
  }
}

class OffersScreenState extends State<OffersScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.unSelectedIndicatorColor,
      appBar: AppBar(
          backgroundColor: MyColors.next_btn_color,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.whiteColor,
            ),
          ),
          title: Text(Strings.offer),
          centerTitle: true),
      body: offersView(),
    );
  }

  // make view for offers View
  Widget offersView() {
    return ListView.builder(
        itemCount: 10,
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.only(top: 8, bottom: 8, right: 8, left: 8),
        itemBuilder: (context, index) {
          return offersItemRow(index);
        });
  }

  // make item row for offers item
  Widget offersItemRow(int index) {
    return Card(
        elevation: 4,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  left: 16, right: 16, top: 10, bottom: 10),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width / 3.8,
                    height: MediaQuery.of(context).size.width / 3.8,
                    decoration: BoxDecoration(
                        color: MyColors.whiteColor,
                        borderRadius: BorderRadius.circular(10),
                        shape: BoxShape.rectangle,
                        image: DecorationImage(
                          image: AssetImage('assets/icons/rohit.jpeg'),
                          fit: BoxFit.cover,
                        )),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text(
                        'Flat 30% off on dry clean services.',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(18),
                            fontWeight: FontWeight.w700,
                            color: MyColors.blackColor),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Coupon code',
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: MyColors.messageColor,
                            fontSize: ScreenUtil().setSp(14)),
                      ),
                      Text(
                        'LAUNDRY30DRY',
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: MyColors.messageColor,
                            fontSize: ScreenUtil().setSp(16)),
                      )
                    ],
                  ))
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                color: MyColors.unSelectedIndicatorColor.withOpacity(0.3),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(8),
                  bottomRight: Radius.circular(8),
                )
              ),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Text(
                      'Valid till 01 Nov 2020',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(18),
                          fontWeight: FontWeight.w500,
                          color: MyColors.next_btn_color),
                    ),
                  ),
                  Text(
                    'Details',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(18),
                        fontWeight: FontWeight.normal,
                        color: MyColors.next_btn_color),
                  ),
                  Icon(
                    Icons.keyboard_arrow_down,
                    color: MyColors.next_btn_color,
                  )
                ],
              ),
            )
          ],
        ));
  }
}
