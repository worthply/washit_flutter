import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/tab_view_pages/about.dart';
import 'package:washit/tab_view_pages/review.dart';
import 'package:washit/tab_view_pages/services.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';

class AboutUsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AboutUsScreenState();
  }
}

class AboutUsScreenState extends State<AboutUsScreen> {
  var top = 0.0;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        body: NestedScrollView(
            headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  bottom: TabBar(
                      unselectedLabelColor:
                          MyColors.messageColor.withOpacity(0.6),
                      labelColor: MyColors.whiteColor,
                      indicatorColor: MyColors.next_btn_color,
                      labelStyle: TextStyle(fontSize: ScreenUtil().setSp(18)),
                      indicatorWeight: 4,
                      tabs: [
                        Tab(
                          child: Text('Services'),
                        ),
                        Tab(
                          text: 'About',
                        ),
                        Tab(
                          text: 'Reviews',
                        ),
                      ]),
                  expandedHeight: 200.0,
                  floating: false,
                  pinned: true,
                  centerTitle: true,
                  title: Text(Strings.aboutUs,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: ScreenUtil().setSp(18),
                      )),
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                        color: MyColors.whiteColor,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      }),
                  backgroundColor: MyColors.next_btn_color,
                  flexibleSpace: LayoutBuilder(
                    builder: (BuildContext context, BoxConstraints constraints) {
                      top = constraints.biggest.height;
                      print('Top Height $top');
                      return FlexibleSpaceBar(
                          background: Image.network(
                        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSCRXvJTFqpOD5ItJfwlRyJ_tM00IcNyaw9S4lwZnB3-oV9NaW7Ew&s",
                        fit: BoxFit.cover,
                      ));
                    },
                  ),
                ),
              ];
            },
            body: aboutUsView()),
      ),
    );
  }

  // make view for about us
  Widget aboutUsView() {
    return Container(
      color: MyColors.whiteColor,
      child: Column(
        children: <Widget>[
          /*TabBar(
              unselectedLabelColor: MyColors.messageColor.withOpacity(0.6),
              labelColor: MyColors.next_btn_color,
              indicatorColor: MyColors.next_btn_color,
              labelStyle: TextStyle(fontSize: ScreenUtil().setSp(18)),
              indicatorWeight: 4,
              tabs: [
                Tab(
                  child: Text('Services'),
                ),
                Tab(
                  text: 'About',
                ),
                Tab(
                  text: 'Reviews',
                ),
              ]),*/
          Expanded(
            child: TabBarView(
              children: <Widget>[
                Services(),
                About(),
                Review(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
