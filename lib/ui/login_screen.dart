import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/ui/home_screen.dart';
import 'package:washit/ui/intro_screen.dart';
import 'package:washit/ui/register_screen.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  TextEditingController emailCon = TextEditingController();
  TextEditingController passwordCon = TextEditingController();

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);
    return SafeArea(
      top: true,
      bottom: true,
      child: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/icons/splash.png'),
                  fit: BoxFit.cover)),
          child: Flex(
            direction: Axis.vertical,
            children: <Widget>[
              Flexible(
                child: ListView(
                  shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  children: <Widget>[
                    loginView(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // make view for login screen
  Widget loginView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 50, bottom: 50),
          child: Image(
            image: AssetImage('assets/icons/splash_icon.png'),
          ),
        ),
        Center(
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(20),
                //height: 280,
                child: Card(
                  elevation: 4,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.white70, width: 1),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          Strings.EMAIL_ID,
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: ScreenUtil().setSp(18),
                              color: MyColors.blackColor),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        TextField(
                          controller: emailCon,
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(16),
                              fontWeight: FontWeight.normal,
                              color: MyColors.greyColor),
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: MyColors.greyColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: MyColors.greyColor),
                              ),
                              hintText: 'Enter Email',
                              hintStyle: TextStyle(
                                  fontSize: ScreenUtil().setSp(16),
                                  fontWeight: FontWeight.normal,
                                  color: MyColors.greyColor)),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          Strings.PASSWORD,
                          style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: MyColors.blackColor,
                            fontSize: ScreenUtil().setSp(18),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        TextField(
                          controller: passwordCon,
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(16),
                              fontWeight: FontWeight.normal,
                              color: MyColors.greyColor),
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: MyColors.greyColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: MyColors.greyColor),
                              ),
                              hintText: 'Enter Password',
                              hintStyle: TextStyle(
                                  fontSize: ScreenUtil().setSp(16),
                                  fontWeight: FontWeight.normal,
                                  color: MyColors.greyColor)),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Text(
                            Strings.FORGOT_PASSWORD,
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: MyColors.greyColor,
                              fontSize: ScreenUtil().setSp(16),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomeScreen()),
                  );
                },
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    padding: EdgeInsets.all(16),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: MyColors.next_btn_color),
                    child: Icon(
                      Icons.arrow_forward,
                      size: 30,
                      color: MyColors.whiteColor,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        SizedBox(
          height: 30,
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 50),
            child: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RegisterScreen()),
                );
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    Strings.REGISTER_TXT,
                    style: TextStyle(
                        fontSize: 16,
                        color: MyColors.messageColor,
                        fontWeight: FontWeight.normal),
                  ),
                  Text(
                    " ${Strings.REGISTER}",
                    style: TextStyle(
                        fontSize: 16,
                        color: MyColors.blackColor,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
