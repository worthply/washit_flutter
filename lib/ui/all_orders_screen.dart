import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/tab_view_pages/ongoing_orders.dart';
import 'package:washit/tab_view_pages/past_orders.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';

class AllOrderScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AllOrderScreenState();
  }
}

class AllOrderScreenState extends State<AllOrderScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.unSelectedIndicatorColor,
      appBar: AppBar(
          backgroundColor: MyColors.next_btn_color,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.whiteColor,
            ),
          ),
          actions: <Widget>[
            IconButton(
                icon: Icon(
                  Icons.search,
                  color: MyColors.whiteColor,
                ),
                onPressed: () {})
          ],
          title: Text(Strings.all_orders),
          centerTitle: true),
      body: allOrderView(),
    );
  }

  // make view for all orders
  Widget allOrderView() {
    return Container(
      color: MyColors.whiteColor,
      child: DefaultTabController(
        length: 2,
        child: Column(
          children: <Widget>[
            TabBar(
                unselectedLabelColor: MyColors.messageColor,
                labelColor: MyColors.next_btn_color,
                indicatorColor: MyColors.itemPriceColor,
                labelStyle: TextStyle(fontSize: ScreenUtil().setSp(16)),
                indicatorWeight: 2,
                tabs: [
                  Tab(
                    child: Text('Ongoing Orders'),
                  ),
                  Tab(
                    text: 'Past Orders',
                  ),
                ]),
            Container(
              width: double.infinity,
              height: 1,
              color: MyColors.greyColor,
            ),
            Expanded(
              child: TabBarView(
                children: <Widget>[
                  OnGoingOrdersScreen(),
                  PastOrderScreen(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
