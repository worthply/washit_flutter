import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/ui/date_time_screen.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';

class SchedulePickUpScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SchedulePickUpScreenState();
  }
}

class SchedulePickUpScreenState extends State<SchedulePickUpScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.whiteColor,
      bottomNavigationBar: InkWell(
        onTap: () {
          Navigator.push(context,
             MaterialPageRoute(builder: (context)=>DateTimeScreen()),);
        },
        child: Container(
            width: double.infinity,
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle, color: MyColors.next_btn_color),
            child: Text(
              Strings.continueDateTime,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: MyColors.whiteColor,
                  fontSize: ScreenUtil().setSp(18)),
            )),
      ),
      appBar: AppBar(
          backgroundColor: MyColors.next_btn_color,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.whiteColor,
            ),
          ),
          title: Text(Strings.schedule_pickup),
          centerTitle: true),
      body: schedulePickUpView(),
    );
  }

  // make view for bottom
  Widget bottomView() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
            width: double.infinity,
            padding: EdgeInsets.only(top: 16, bottom: 16, left: 16, right: 16),
            decoration: BoxDecoration(color: MyColors.whiteColor, boxShadow: [
              BoxShadow(
                color: Colors.black,
                offset: Offset(1.0, 6.0),
                blurRadius: 10.0,
              ),
            ]),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextField(
                  decoration: InputDecoration(
                      hintText: 'Address Line 1',
                      alignLabelWithHint: true,
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: MyColors.greyColor,
                          fontSize: ScreenUtil().setSp(20))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextField(
                  decoration: InputDecoration(
                      hintText: 'Address Line 2',
                      alignLabelWithHint: true,
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: MyColors.greyColor,
                          fontSize: ScreenUtil().setSp(20))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextField(
                  decoration: InputDecoration(
                      hintText: 'Address Title - i.e. office, home etc',
                      alignLabelWithHint: true,
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: MyColors.greyColor,
                          fontSize: ScreenUtil().setSp(20))),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            )),
      ],
    );
  }

  // make view for schedule pickup
  Widget schedulePickUpView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: double.infinity,
          padding: EdgeInsets.only(left: 40, right: 40, top: 8, bottom: 8),
          decoration: BoxDecoration(
              color: MyColors.whiteColor,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(8),
                bottomRight: Radius.circular(8),
              )),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(image: AssetImage('assets/icons/location.png')),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Location',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(10),
                        color: MyColors.blackColor.withOpacity(0.8),
                        fontWeight: FontWeight.w800),
                  )
                ],
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: Container(
                  height: 1,
                  color: MyColors.greyColor,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(
                    image: AssetImage('assets/icons/date_blur.png'),
                    height: 40,
                    width: 40,
                    fit: BoxFit.fill,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Date/Time',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(10),
                        color: MyColors.blackColor.withOpacity(0.8),
                        fontWeight: FontWeight.w800),
                  )
                ],
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: Container(
                  height: 1,
                  color: MyColors.greyColor,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(image: AssetImage('assets/icons/payment_blur.png')),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Payment',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(10),
                        color: MyColors.blackColor.withOpacity(0.8),
                        fontWeight: FontWeight.w800),
                  )
                ],
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: Container(
                  height: 1,
                  color: MyColors.greyColor,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(image: AssetImage('assets/icons/complete_blur.png')),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Complete',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(10),
                        color: MyColors.blackColor.withOpacity(0.8),
                        fontWeight: FontWeight.w800),
                  )
                ],
              ),
            ],
          ),
        ),
        Container(
          width: double.infinity,
          height: 1.5,
          color: MyColors.greyColor.withOpacity(0.7),
        ),
        Expanded(
            child: Container(
          color: MyColors.whiteColor,
        )),
        bottomView(),
      ],
    );
  }
}
