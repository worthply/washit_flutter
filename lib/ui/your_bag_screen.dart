import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/ui/schedule_pickup_screen.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';

class YourBagScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return YourBagScreenState();
  }
}

class YourBagScreenState extends State<YourBagScreen> {
  bool isTappedPlus = false;
  bool isTappedMinus = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.whiteColor,
      bottomNavigationBar: Container(
          width: double.infinity,
          color: MyColors.whiteColor,
          child: bottomView()),
      appBar: AppBar(
          backgroundColor: MyColors.next_btn_color,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: MyColors.whiteColor,
            ),
          ),
          title: Text(Strings.yourBag),
          centerTitle: true),
      body: yourBagView(),
    );
  }

  // make view for bottom view
  Widget bottomView() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: double.infinity,
          padding: EdgeInsets.only(top: 8, bottom: 8, left: 16, right: 16),
          color: MyColors.bagItemColor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Total Price',
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        color: MyColors.whiteColor,
                        fontSize: ScreenUtil().setSp(18),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      '25 Items',
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        color: MyColors.whiteColor.withOpacity(0.6),
                        fontSize: ScreenUtil().setSp(14),
                      ),
                    ),
                  ],
                ),
              ),
              Text(
                '\$99 + 17.99 Tax',
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  color: MyColors.whiteColor,
                  fontSize: ScreenUtil().setSp(18),
                ),
              ),
            ],
          ),
        ),
        InkWell(
          onTap: () {
            Navigator.push(context,
            MaterialPageRoute(builder: (context)=>SchedulePickUpScreen()),);
          },
          child: Container(
              width: double.infinity,
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle, color: MyColors.next_btn_color),
              child: Text(
                Strings.schedulePickUp,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: MyColors.whiteColor,
                    fontSize: ScreenUtil().setSp(18)),
              )),
        ),
      ],
    );
  }

  // make view for bag
  Widget yourBagView() {
    return ListView.builder(
      itemCount: 3,
      itemBuilder: (context, index) {
        return washingCategoryRow(index);
      },
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
    );
  }

  // make row for washing category item
  Widget washingCategoryRow(int indexMain) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 16),
                child: Text(
                  indexMain == 0 ? 'Washup & Fold' : 'Wash & Iron',
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      color: MyColors.blackColor.withOpacity(0.8),
                      fontSize: ScreenUtil().setSp(18)),
                ),
              ),
            ),
            IconButton(
              icon: Icon(Icons.edit, color: MyColors.blackColor),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.delete, color: MyColors.blackColor),
              onPressed: () {},
            ),
          ],
        ),
        Container(
          width: double.infinity,
          height: 0.9,
          color: MyColors.blackColor,
        ),
        ListView.builder(
            shrinkWrap: true,
            itemCount: 4,
            physics: ClampingScrollPhysics(),
            itemBuilder: (context, index) {
              return categoryItemRow(index,indexMain);
            })
      ],
    );
  }

  // make row for category item
  Widget categoryItemRow(int index,int indexMain) {
    return Padding(
      padding: const EdgeInsets.only(left: 16, top: 8, bottom: 8, right: 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              InkWell(
                onTap: () {
                  setState(() {
                    if (index == 0 && indexMain == 0) {
                      if (isTappedMinus) {
                        isTappedMinus = false;
                        isTappedPlus = true;
                      } else {
                        isTappedMinus = true;
                      }
                    }
                  });
                },
                child: index == 0
                    ? isTappedMinus
                        ? Image(
                            image: AssetImage('assets/icons/minus.png'),
                            height: 25,
                            width: 25,
                          )
                        : Image(
                            image: AssetImage('assets/icons/plus.png'),
                            height: 25,
                            width: 25,
                          )
                    : Image(
                        image: AssetImage('assets/icons/plus.png'),
                        height: 25,
                        width: 25,
                      ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'Men\'s',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(18),
                    color: MyColors.blackColor.withOpacity(0.8)),
              )
            ],
          ),
          index == 0
              ? isTappedMinus
                  ? Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 50),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(flex: 2, child: Container()),
                              Expanded(
                                flex: 1,
                                child: Text(
                                  'Unit Price',
                                  style: TextStyle(
                                      fontSize: ScreenUtil().setSp(12),
                                      fontWeight: FontWeight.normal,
                                      color: MyColors.greyColor),
                                ),
                              ),
                              Text(
                                'Total Price',
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(12),
                                    fontWeight: FontWeight.normal,
                                    color: MyColors.greyColor),
                              )
                            ],
                          ),
                        ),
                        ListView.builder(
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                            itemCount: 4,
                            itemBuilder: (context, index) {
                              return productItemRow(index);
                            }),
                      ],
                    )
                  : Container()
              : Container()
        ],
      ),
    );
  }

  // make row for product item row
  Widget productItemRow(int index) {
    return Padding(
      padding: const EdgeInsets.only(left: 50, top: 2, bottom: 5),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
              flex: 2,
              child: Text(
                '2 X T-Shirt',
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: MyColors.blackColor.withOpacity(0.8),
                    fontSize: ScreenUtil().setSp(18)),
              )),
          Expanded(
              flex: 1,
              child: Text('\$5.00',
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: MyColors.blackColor.withOpacity(0.8),
                      fontSize: ScreenUtil().setSp(18)))),
          Text('\$10.00',
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  color: MyColors.blackColor.withOpacity(0.8),
                  fontSize: ScreenUtil().setSp(18))),
        ],
      ),
    );
  }
}
