import 'dart:async';
import 'package:flutter/material.dart';
import 'package:washit/ui/login_screen.dart';
import 'package:washit/utils/Strings.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/utils/my_colors.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    startSplashScreenTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);
    return Scaffold(
      body: splashView(),
    );
  }

  // make view for splash screen
  Widget splashView() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/icons/splash.png'), fit: BoxFit.cover),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image(
            image: AssetImage('assets/icons/splash_icon.png'),
            fit: BoxFit.cover,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            Strings.WASH_IT,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(40),
                fontWeight: FontWeight.normal,
                color: MyColors.blackColor),
          ),
        ],
      ),
    );
  }

  // start timer for splash screen
  startSplashScreenTimer() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationToNextPage);
  }

  // navigate to next screen
  navigationToNextPage() async {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginScreen()),
    );
  }
}
