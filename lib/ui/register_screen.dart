import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:washit/ui/login_screen.dart';
import 'package:washit/utils/Strings.dart';
import 'package:washit/utils/my_colors.dart';

class RegisterScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RegisterScreenState();
  }
}

class RegisterScreenState extends State<RegisterScreen> {
  TextEditingController emailCon = TextEditingController();
  TextEditingController passwordCon = TextEditingController();
  TextEditingController usernameCon = TextEditingController();
  TextEditingController phoneCon = TextEditingController();

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    return SafeArea(
      top: true,
      bottom: true,
      child: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/icons/splash.png'),
                  fit: BoxFit.cover)),
          child: ListView(
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            children: <Widget>[
              registerView(),
            ],
          ),
        ),
      ),
    );
  }

  // make view for register user
  Widget registerView() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 50, bottom: 50),
          child: Image(
            image: AssetImage('assets/icons/splash_icon.png'),
          ),
        ),
        Center(
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(20),
                //height: 280,
                child: Card(
                  elevation: 4,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.white70, width: 1),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          Strings.USER_NAME,
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: ScreenUtil().setSp(18),
                              color: MyColors.blackColor),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        TextField(
                          controller: usernameCon,
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(16),
                              fontWeight: FontWeight.normal,
                              color: MyColors.greyColor),
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: MyColors.greyColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: MyColors.greyColor),
                              ),
                              hintText: 'Enter name',
                              hintStyle: TextStyle(
                                  fontSize: ScreenUtil().setSp(16),
                                  fontWeight: FontWeight.normal,
                                  color: MyColors.greyColor)),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          Strings.PHONE_NUMBER,
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: ScreenUtil().setSp(18),
                              color: MyColors.blackColor),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        TextField(
                          controller: phoneCon,
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(16),
                              fontWeight: FontWeight.normal,
                              color: MyColors.greyColor),
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: MyColors.greyColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: MyColors.greyColor),
                              ),
                              hintText: 'Enter phone number',
                              hintStyle: TextStyle(
                                  fontSize: ScreenUtil().setSp(16),
                                  fontWeight: FontWeight.normal,
                                  color: MyColors.greyColor)),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          Strings.EMAIL_ID,
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: ScreenUtil().setSp(18),
                              color: MyColors.blackColor),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        TextField(
                          controller: emailCon,
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(16),
                              fontWeight: FontWeight.normal,
                              color: MyColors.greyColor),
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: MyColors.greyColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: MyColors.greyColor),
                              ),
                              hintText: 'Enter Email',
                              hintStyle: TextStyle(
                                  fontSize: ScreenUtil().setSp(16),
                                  fontWeight: FontWeight.normal,
                                  color: MyColors.greyColor)),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          Strings.PASSWORD,
                          style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: MyColors.blackColor,
                            fontSize: ScreenUtil().setSp(18),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        TextField(
                          controller: passwordCon,
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(16),
                              fontWeight: FontWeight.normal,
                              color: MyColors.greyColor),
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: MyColors.greyColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: MyColors.greyColor),
                              ),
                              hintText: 'Enter Password',
                              hintStyle: TextStyle(
                                  fontSize: ScreenUtil().setSp(16),
                                  fontWeight: FontWeight.normal,
                                  color: MyColors.greyColor)),
                        ),
                        SizedBox(
                          height: 50,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: MyColors.next_btn_color),
                  child: Icon(
                    Icons.arrow_forward,
                    size: 30,
                    color: MyColors.whiteColor,
                  ),
                ),
              )
            ],
          ),
        ),
        SizedBox(
          height: 30,
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 50),
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    Strings.LOGIN_TXT,
                    style: TextStyle(
                        fontSize: 16,
                        color: MyColors.messageColor,
                        fontWeight: FontWeight.normal),
                  ),
                  Text(
                    " ${Strings.LOGIN}",
                    style: TextStyle(
                        fontSize: 16,
                        color: MyColors.blackColor,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
