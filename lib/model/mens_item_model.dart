import 'package:flutter/material.dart';

class MenItemModel {
  String image = "";
  String itemName = "";
  String itemPrice = "";

  MenItemModel(this.image, this.itemName, this.itemPrice);
}
