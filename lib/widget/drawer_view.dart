import 'package:flutter/material.dart';

class DrawerView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return DrawerViewState();
  }
}

class DrawerViewState extends State<DrawerView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: drawerView(),
    );
  }

  // make view for drawer layout
  Widget drawerView() {
    return Container(
      height: MediaQuery.of(context).size.height/2.5,
      width: double.infinity,
      padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
      decoration: BoxDecoration(
        color: Colors.black54,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[

                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
