import 'package:flutter/material.dart';

class Strings {
  static final String WASH_IT = 'WashIt';
  static final String EMAIL_ID = 'Email Id';
  static final String USER_NAME = 'Username';
  static final String PHONE_NUMBER = 'Phone Number';
  static final String PASSWORD = 'Password';
  static final String FORGOT_PASSWORD = 'Forgot Password?';
  static final String REGISTER_TXT = "Don't have an account?";
  static final String LOGIN_TXT = "Already have an account?";
  static final String REGISTER = "Register";
  static final String LOGIN = "Login";

  // view pager item text
  static final String choose_cloths = 'Choose Your Cloths';
  static final String schedule_pickup = 'Schedule Pickup';
  static final String washing_facilities = 'Get Top Washing Facilities';
  static final String time_delivery = 'Get On-Time Delivery';
  static final String feel_satisfied = 'Pay Later & Feel Satisfied';

  // Home Screen
  static final String home = 'Home';
  static final String all_orders = 'All Orders';
  static final String notification = 'Notification';
  static final String rafer_and_earn = 'Rafer & Earn';
  static final String about_us = 'About us';
  static final String settings = 'Settings';
  static final String services = 'SERVICES';
  static final String menu = 'Menu';

  // Refer and earn screen
  static final String refer_and_earn_free_wash = 'Refer & Earn a Free Wash';

  // Profile Screen
  static final String profile = 'Profile';
  static final String userName = 'Username';
  static final String phoneNumber = 'Phone Number';
  static final String emailId = 'Email Id';

  // Rate order Screen
  static final String rateOrder = 'Rate Order';
  static final String submit = 'Submit';

  // offers screen
  static final String offer = "Offers";

  // About us screen
  static final String aboutUs = "About Us";

  // All order screen
  static final String allOrders = "All Orders";

  // Order details screen
  static final String orderDetails = "Order Details";

  // Your Bag Screen
  static final String yourBag = "Your Bag";
  static final String schedulePickUp = "Schedule Pickup";
  static final String continueDateTime = "Continue to Date/Time";
  static final String continuePaymentMethod = "Continue to Payment Method";
  static final String confirm_order = "Confirm Order";
  static final String orderStatus = "Go To Order Status";
}
