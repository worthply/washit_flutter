import 'package:flutter/material.dart';

class MyColors {
  static final Color greyColor = Colors.grey;
  static final Color whiteColor = Colors.white;
  static final Color blackColor = Colors.black;
  static final Color blueColor = Colors.blue.shade800;
  static final Color itemPriceColor = Color(0xFFCA7D27);
  static final Color bagItemColor = Color(0xFF2C2C2C);
  static final Color messageColor = Color(0xFF161616);
  static final Color next_btn_color = Color(0xFFFF6A00);
  static final Color ratingPercentColor = Color(0xFFFCAA08);
  static final Color unSelectedIndicatorColor = Color(0xFFDDDDDD);
  static final Color referAndContainerColor = Color(0xFFF7F7F7);
  static final Color orderSuccessColor = Color(0xFF37B188);
}